from django.http import HttpResponse
from django.db.models import Sum

from ..models import Signature, Initiative, CountryInitiative

import json
import requests
import datetime


def daily(request):
    initiative = Initiative.objects.get(slug=request.GET.get('initiative', ''))

    for x in Signature.objects.filter(country_initiative__initiative=initiative):
        pass
    data = None

    return HttpResponse(json.dumps(data), mimetype="application/json")

def total(request):
    today = datetime.date.today()
    initiative = Initiative.objects.get(slug=request.GET.get('initiative', ''))
    # r = requests.get(initiative.data_url)
    # return HttpResponse(json.dumps(r.json()), mimetype="application/json")

    data = []
    data.append(['Country',
                 'Total % quota', 'Total Signatures', 'Daily Signatures' ])
    qs = CountryInitiative.objects.select_related().filter(initiative=initiative)
    for x in qs:
        daily = Signature.objects.filter(country_initiative=x, timestamp__gte=today).aggregate(
            daily_count=Sum('count'))
        data.append([
            x.country.name,
            float(x.current_percentage or 0), x.current_count,
            daily['daily_count'] or 0 ])

    return HttpResponse(json.dumps(data), mimetype="application/json")
