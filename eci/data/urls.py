from django.conf.urls import patterns, url

from .views import daily, total


urlpatterns = patterns('',
    url(r'^daily/$', daily, name="eci:data_total"),
    url(r'^total/$', total, name="eci:data_daily"),
)
