from django.core.management.base import BaseCommand, CommandError
from ...models import Initiative


class Command(BaseCommand):
    help = "Fetches and writes ECI data"

    def handle(self, *args, **kwargs):
        for x in Initiative.objects.filter(status=Initiative.STATUS_ONGOING):
            x.write_current_data()
