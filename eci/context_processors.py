from .models import Initiative


def initiatives(request):
    return {'initiatives': Initiative.objects.all()}

def countries(request):
    return {'countries': Country.objects.all()}
