from django.shortcuts import render
from .models import Initiative


def initiative(request, initiative_slug):
    initiative = Initiative.objects.get(slug=initiative_slug)
    ctx = {
        'initiative': initiative
    }
    return render(request, 'eci/view.html', ctx)
