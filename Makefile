clean:
	rm -rf dist/ django_eci.egg-info/
	find . -name "*.pyc" -delete
	find . -name "*.orig" -delete

sdist:
	python setup.py sdist

upload:
	python setup.py sdist upload
